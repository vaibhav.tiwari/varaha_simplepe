import numpy as np
from scipy.stats import norm
from scipy.integrate import quad
from scipy.interpolate import interp1d, CubicSpline
from scipy.special import logsumexp
from scipy.stats import percentileofscore

import time
from copy import deepcopy
from multiprocessing import Pool, cpu_count

import detectors, gnobs
from conversions import *

from pycbc import waveform
from pycbc.detector import Detector
from pycbc.waveform import get_fd_waveform
import sigproc
#from pycbc.filter import sigmasq, match, matched_filter
from pycbc.filter.matchedfilter import make_frequency_series
from pycbc.psd import interpolate

from importlib import reload
reload(sigproc)

max_spin = 0.9
apx = 'IMRPhenomXPHM'
#apx = 'IMRPhenomD'

def get_flow_for_length(template_param, length, f_low = 20.):
   
    m1_det, m2_det, s1zt, s2zt = template_param
    for f in np.arange(f_low, 100., 1):
        h1, _ = get_fd_waveform(approximant = 'IMRPhenomD',
                             mass1 = m1_det, mass2 = m2_det,
                               spin1z = s1zt, spin2z = s2zt,
                             f_lower = f, delta_f = 1/length, f_final = 100.)
   
        idxlow = int(f * length)
        idxhigh = int((f + 10.) * length)
        phase = np.unwrap(np.angle(h1[idxlow:idxhigh]))
        dphase = np.diff(phase)

        if len(dphase[dphase < 0]) == 0:
            return f
       
def get_length_for_flow(template_param, flow):
   
    m1_det, m2_det, s1zt, s2zt = template_param
    lengths = 2 ** np.arange(2, 9)
    
    for length in lengths:
        h1, _ = get_fd_waveform(approximant = 'IMRPhenomD',
                             mass1 = m1_det, mass2 = m2_det,
                               spin1z = s1zt, spin2z = s2zt,
                             f_lower = flow, delta_f = 1./length, f_final = 100)
   
        idxlow = int((flow - 5) * length)
        idxhigh = int((flow + 5.) * length)
        phase = np.unwrap(np.angle(h1[idxlow:idxhigh]))
        dphase = np.diff(phase)

        if len(dphase[dphase < 0]) == 0:
            return length

def prepare_strain_data(segment, ts, gps, psd):
   
    net = list(ts.keys())
    ts_ft, psd_seg = {}, {}
    for det in net:
        t0, delta_t = ts[det].sample_times[0], ts[det].delta_t
        Ds = segment[1] - segment[0]
        idxstart = int((segment[0] - t0)/delta_t)
        idxstop = idxstart + int(Ds/delta_t)

        tsft = (ts[det][idxstart:idxstop]).to_frequencyseries()
        psdseg = interpolate(psd[det], tsft.delta_f)
           
        ts_ft[det] = tsft
        psd_seg[det] = psdseg
       
    return ts_ft, psd_seg

def assemble_args_extrinsic(signal_tilde, template_param, psd, freqs, gps, Dt):
    ''' Assemble the arguments needed to perform the parameter estimation'''
   
    signal_tilde = deepcopy(signal_tilde)
    psd = deepcopy(psd)
    net = list(signal_tilde.keys())
    m1, m2, s1z, s2z = template_param
   
    det = net[0]
    df = psd[det].delta_f
    flen = len(psd[det])
    hp22, _ = get_fd_waveform(approximant='IMRPhenomD', mass1=m1, mass2=m2,
    spin1z=s1z, spin2z=s2z, delta_f=df,
    f_lower=freqs[0], f_final = freqs[-1])
    hp22.resize(flen)
    
    series_times = signal_tilde[det].to_timeseries().sample_times.data
    snr_dt = series_times[1] - series_times[0]
    istart = int((gps - series_times[0] - Dt) / snr_dt)
    istop = int((gps - series_times[0] + Dt) / snr_dt)
    series_times = series_times[istart:istop]

    maxdetsnr = 0
    hp22_dot_hp22, hp22_dot_d, max_det_snr = {}, {}, {}
    for det in net:
        hp22_dot_hp22[det] = sigproc.sigmasq(hp22.data, psd[det].data, df)
        hp22_dot_d[det] = sigproc.matched_filter(hp22.data, signal_tilde[det].data, df)
            
        hp22_dot_d[det] = hp22_dot_d[det][istart:istop]
        abs_snr_series = np.abs(hp22_dot_d[det] / np.sqrt(hp22_dot_hp22[det]))

        max_snr_idx = np.argmax(abs_snr_series)
        max_det_snr[det] = abs_snr_series[max_snr_idx]
        tc_det = series_times[max_snr_idx]
       
        if max_det_snr[det] > maxdetsnr:
            refdet = det
            tc_refdet = tc_det
            maxdetsnr = max_det_snr[det]

    netsnr = np.sqrt(sum(max_det_snr[det] ** 2 for det in net))
   
   
    args_extrin = {}
    args_extrin['delta_f'] = df
    args_extrin['psd'] = {det:psd[det].data for det in psd.keys()}
    args_extrin['net'] = net
    args_extrin['freqs'] = freqs
    args_extrin['netsnr'] = netsnr
    args_extrin['refdet'] = refdet
    args_extrin['series_times'] = series_times
    args_extrin['istart'] = istart
    args_extrin['istop'] = istop
    args_extrin['tc_refdet'] = tc_refdet
    args_extrin['hp22_dot_hp22'] = hp22_dot_hp22
    args_extrin['hp22_dot_d'] = hp22_dot_d
    args_extrin['max_det_snr'] = max_det_snr
    args_extrin['signal_tilde']  = {det:signal_tilde[det].data for det in psd.keys()}

    args_extrin['lkl_thr'] = -1e9
    args_extrin['phi_lkl_thr'] = 0.5 * netsnr ** 2 - 20

    return args_extrin

def assemble_param_ranges_extrinsic(args_extrinsic):
    '''Initialize parameter ranges for localising volume(extrinsic)'''
    
    hp22_dot_hp22 = args_extrinsic['hp22_dot_hp22']
    hp22_dot_d = args_extrinsic['hp22_dot_d']
    series_times = args_extrinsic['series_times']
    refdet = args_extrinsic['refdet']
    
    ss = np.abs(hp22_dot_d[refdet] / np.sqrt(hp22_dot_hp22[refdet]))
    argmax = np.argmax(ss)
    sssq = ss ** 2 - ss[argmax] ** 2
    idxsel = np.where(sssq > -16)
    idxl = max(0, min(argmax - 2, idxsel[0][0] - 1))
    idxr = min(len(sssq) - 1, max(argmax + 2, idxsel[0][-1] + 1))
    ssinterp = interp1d(series_times[idxl:idxr + 1], sssq[idxl:idxr + 1], kind='quadratic', bounds_error = False, fill_value = -25)
    ax = np.linspace(series_times[idxl], series_times[idxr], (idxr - idxl) * 10)
    ssin = ssinterp(ax)
    idxsel = np.where(ssin > -16)
    l, r = ax[max(0, idxsel[0][0] - 1)], ax[min(len(ax) - 1, idxsel[0][-1] + 1)]
    tc_range = [l, r]
   
    refdet, netsnr = args_extrinsic['refdet'], args_extrinsic['netsnr']
    horizon = np.sqrt(args_extrinsic['hp22_dot_hp22'][refdet])
    lumd_max = 2 * horizon / args_extrinsic['max_det_snr'][refdet]
   
    params = {}
    params['x'] = ['lumd_cube', 'cosi', 'ra', 'sindec', 'tc', 'psi']
    params['xrange'] = [[0, lumd_max ** 3], [-1, 1], [0, 2*np.pi], [-1, 1], tc_range, [0, 2*np.pi]]
    params['xrange'] = np.array(params['xrange'])
   
    return params

def assemble_args_intrinsic(args_extrin, extrin_mc, template_param):
   
    args_intrin = deepcopy(args_extrin)
   
    net = args_extrin['net']
    tc_refdet = args_extrin['tc_refdet']
    netsnr = args_extrin['netsnr']
   
    lkl = extrin_mc['log_likelihood']
    ra_mc, dec_mc = extrin_mc['ra'], extrin_mc['dec']
    lumd_mc, cosi_mc = extrin_mc['lumd'], extrin_mc['cosi']
    psi_mc = extrin_mc['psi']
    nsel = 200000
    tc = extrin_mc['tc_refdet']
    ra_mc = ra_mc[:nsel]
    dec_mc = dec_mc[:nsel]
    lumd_mc = lumd_mc[:nsel]
    cosi_mc = cosi_mc[:nsel]
    psi_mc = np.random.uniform(0, 2 * np.pi, nsel)
   
    fp, fc = detectors.get_fp_fc(net, ra_mc, dec_mc, psi_mc, tc_refdet)
    dt_dets_mc = {}
    A22, A33 = {}, {}
    #Generate multiple sets of fiducial extrinsic parameters
    for kk in range(10):
        A22[kk], A33[kk], dt_dets_mc[kk] = {}, {}, {}
        for det in net:
            a, b = kk*20000, (kk+1)*20000
            A22[kk][det] = fp[det][a:b] * (1 + cosi_mc[a:b] ** 2) / 2 + 1j * fc[det][a:b] * cosi_mc[a:b]
            A22[kk][det] /= lumd_mc[a:b]
            A33[kk][det] = A22[kk][det] * 2 * np.sin(cosi_mc[a:b])
            dt_dets_mc[kk][det] = detectors.time_delay_between_obs(args_extrin['refdet'],
                                                            det, ra_mc[a:b], dec_mc[a:b], tc_refdet)

    args_intrin['A22'] = A22
    args_intrin['A33'] = A33
    args_intrin['dt_dets_mc'] = dt_dets_mc
    args_intrin['lkl_thr'] = 0.75 * np.min(lkl)
    args_intrin['delta_tc'] = np.max(tc) - np.min(tc)
    args_intrin['max_lkl_full'] = np.max(lkl)
    
    return args_intrin
   
def assemble_param_ranges_intrinsic(args_extrinsic, template_param):
    
    m1, m2, s1z, s2z = template_param
    netsnr = args_extrinsic['netsnr']
    
    params = {}
    max_spin = 0.9
    max_dPsi1D = 20.
    qmin, qmax = 0.1, 1.0
    m1, m2, s1z, s2z = template_param
    mch, q = m1m2_to_mchq(m1, m2)

    #mch_std: 50 times the Eq 28 in arXiv:gr-qc/9402014
    mch_std = 50. * 1.2 * 1e-5 * (10/netsnr) * mch ** (8./3)
    mch_std = min(mch_std, mch ** 1.1 / 20.)
    # Here we provide phase difference between two frequencies to encapsulate phase evolution
    params['Freq'] = [20., 1000.]
   
    Psiatl = get_pn_phase(m1, m2, s1z, s2z, params['Freq'][0])
    PsiatFreq = Psiatl - get_pn_phase(m1, m2, s1z, s2z, params['Freq'][-1])
    params['Psi0'] = PsiatFreq
        
    min_mch, max_mch = mch - 5 * mch_std, mch + 5 * mch_std
    params['x'] = ['mchirp', 'q', 'spin1z', 'spin2z']
    params['xrange'] = [[min_mch, max_mch], [qmin, qmax], [-max_spin, max_spin], [-max_spin, max_spin]]
    params['xrange'] = np.array(params['xrange'])

    params['max_dPsi1D'] = max_dPsi1D
   
    return params

def override_intrinsic_ranges(args_intrin, params, intrinsic_mc, nsel, V, trunc_p, at_final_threshold):
    '''
       Converge in the intrinsic parameter space.
    '''
   
    enc_prob = 0.995
    keys = list(intrinsic_mc.keys())
    allx = np.vstack([intrinsic_mc[key]['x'] for key in keys])
    all_lkl = np.concatenate([intrinsic_mc[key]['log_likelihood'] for key in keys])
    all_dPsi = np.concatenate([intrinsic_mc[key]['dPsi'] for key in keys])
    all_maxl = np.array([intrinsic_mc[key]['maxl_cycle'] for key in keys])

    idxsel = np.where(all_lkl > args_intrin['lkl_thr'])
    
    ndim = allx.shape[1]
    if len(keys) == 1:
        ninj = intrinsic_mc[keys[0]]['niter']
        fac = np.linspace(1, 2, ndim)
        fac *= (ndim/np.sum(fac))
    else:
        ninj = len(all_lkl[idxsel])
        fac = 1/np.sqrt(params['binscale']/np.sum(params['binscale']))
        fac *= (ndim/np.sum(fac))

    if not(at_final_threshold):
        args_intrin['lkl_thr'], truncp = get_likelihood_threshold(all_lkl[idxsel], nsel, 1 - enc_prob - trunc_p)
        trunc_p += truncp   
    at_final_threshold = np.round(enc_prob/trunc_p) - np.round(enc_prob/(1 - enc_prob)) == 0
    
    idxsel = np.where(all_lkl > args_intrin['lkl_thr'])
    allx = allx[idxsel]
    all_lkl = all_lkl[idxsel]
    w = np.exp(all_lkl - np.max(all_lkl))
    neff = np.sum(w) ** 2 / np.sum(w ** 2)
   
    nrec = len(all_lkl)
    V *= (nrec/ninj)
    delta_V = V/np.sqrt(nrec)
    nbins = get_bins(delta_V, fac, ndim)
    params['dx'] = np.diff(params['xrange'], axis = 1).flatten() / nbins
    binidx = ((allx - params['xrange'].T[0]) / params['dx'].T).astype(int)
    
    # A fast way to histogram
    params['binunique'] = np.unique(binidx, axis = 0)
    # Adhoc way to size the bins while mainitaining volume delta_V per bin
    params['binscale'] += np.array([len(np.unique(bu)) for bu in params['binunique'].T])/nbins

    sel_dPsi = all_dPsi[idxsel]
    p = np.percentile(sel_dPsi, 100 - 2 * 100 / np.sqrt(nrec))
    params['max_dPsi1D'] = 2 * sel_dPsi.max() - p
    args_intrin['max_lkl_full'] = max(args_intrin['max_lkl_full'], np.max(all_maxl))
       
    return args_intrin, params, all_lkl, neff, V, nbins, trunc_p, at_final_threshold

def get_network_time_delays(net, refdet, ra, dec, t_gps):
    ''' Get the time delays between the detectors in the network
        compared to a reference detector
    '''
    dt_dets = {}
    for det in net:
        dt_dets[det] = detectors.time_delay_between_obs(refdet, det, ra, dec, t_gps)
   
    return dt_dets

def max_likelihood_extrinsic(net, snr_max_det, hpsigsq_normed):
    raw_chisq = 0
    for det in net:
        raw_chisq -= hpsigsq_normed[det] / 2
        raw_chisq += np.sqrt(hpsigsq_normed[det]) * snr_max_det[det]
   
    return raw_chisq

def snr_at_tc(net, refdet, snr_series, series_times, t_refdet, dt_dets):
    ''' Assemble the data needed to shift the time in the reference detectors
        Also calculate coalescence time in other detectors
    '''
    nsky = len(dt_dets[refdet])
    t0, snr_dt = series_times[0], series_times[1] - series_times[0]
    snrabs_at_time, snrphi_at_time = {}, {}
   
    for det in net:
        t_det = t_refdet - dt_dets[det]
        min_t, max_t = np.min(t_det), np.max(t_det)
        min_idx = ((min_t - t0)/snr_dt).astype(int) - 2
        min_idx = max(0, min_idx)
        max_idx = ((max_t - t0)/snr_dt).astype(int) + 2
        max_idx = min(len(series_times), max_idx)
       
        cplxsnr = snr_series[det][min_idx:max_idx]
       
        abssnr = np.abs(cplxsnr)
        phisnr = np.unwrap(np.angle(cplxsnr))

        abs_at_time = interp1d(series_times[min_idx:max_idx], abssnr, kind='quadratic', bounds_error = False, fill_value = 0.)
        phi_at_time = np.interp(t_det, series_times[min_idx:max_idx], phisnr)
       
        snrabs_at_time[det] = abs_at_time(t_det)
        snrphi_at_time[det] = phi_at_time
     
    return snrabs_at_time, snrphi_at_time

def sky_likelihood(net, h_dot_h, h_dot_d, lkl_thr):
   
    sum_hh = 0
    sum_hd = 0
    for det in net:
        sum_hd += h_dot_d[det]
        sum_hh -= h_dot_h[det]
    sum_hh *= 0.5
   
    # We have ignored phi_c, we can either impart an additional phase to sum_hd for a uniformly sampled phi_c
    # I am alternatively choosing the phase that results a likelihood value between maximum possible and lkl_thr
    # This indirectly sets an unknown prior on phi_c but gives comparable results
    r = np.abs(sum_hd)# Maximise on phase (not phi_c)
    min_cos2phi = (lkl_thr - sum_hh) / r
    max_2phi = np.ones_like(r) * np.pi / 4
    idx = np.where(np.abs(min_cos2phi) < 1.0)
    max_2phi[idx] = np.arccos(min_cos2phi[idx])
    
    twophi = np.linspace(0, max_2phi, 10) #marginalise over phi's range
    lkl = sum_hh + logsumexp(r * np.cos(twophi), axis = 0)
    
    lkl += np.log(max_2phi)#adjust for unequal range of phi's prior

    return lkl

def volume_localized(params_args_extrinsic):
   
    np.random.seed()
    args = deepcopy(params_args_extrinsic)
    args_extrin, params = args
   
    df = args_extrin['delta_f']
    netsnr = args_extrin['netsnr']
    max_det_snr = args_extrin['max_det_snr']
    net, refdet = args_extrin['net'], args_extrin['refdet']
    hp22_dot_hp22 = args_extrin['hp22_dot_hp22']
    hp22_dot_d = args_extrin['hp22_dot_d']
    series_times = args_extrin['series_times']
    tc_refdet = args_extrin['tc_refdet']
    lkl_thr = args_extrin['lkl_thr']
    phi_lkl_thr = args_extrin['phi_lkl_thr']

    dx = params['dx']
    xrange = params['xrange']
    bu = params['binunique']
    ninbin = params['ninbin']
   
    xlo, xhi = xrange.T[0] + dx * bu, xrange.T[0] + dx * (bu+1)
    x = np.vstack([np.random.uniform(xlo[kk], xhi[kk], size = (npb, xlo.shape[1])) for kk, npb in enumerate(ninbin)])
    lumdcube_mc, cosi_mc, ra_mc, sindec_mc, tc_refdet_mc, psi_mc = x.T
    dec_mc = np.arcsin(sindec_mc)
   
    fp, fc = detectors.get_fp_fc(net, ra_mc, dec_mc, psi_mc, tc_refdet)
    A22, h_dot_h = {}, {}
    for det in net:
        A22[det] = fp[det] * (1 + cosi_mc ** 2) / 2 + 1j * fc[det] * cosi_mc 
        A22[det] /= np.cbrt(lumdcube_mc)
        h = np.sqrt(hp22_dot_hp22[det]) * A22[det]
        h_dot_h[det] = sigproc.get_product(h, h)
   
    #Remove samples that are confidently unlikely
    raw_lkl = max_likelihood_extrinsic(net, max_det_snr, h_dot_h)
    idx_snrcut = np.where(raw_lkl > lkl_thr - 1.0)
       
    ra_mc, dec_mc = ra_mc[idx_snrcut], dec_mc[idx_snrcut]
    lumdcube_mc, cosi_mc = lumdcube_mc[idx_snrcut], cosi_mc[idx_snrcut]
    tc_refdet_mc = tc_refdet_mc[idx_snrcut]
    psi_mc = psi_mc[idx_snrcut]
    dt_dets = get_network_time_delays(net, refdet, ra_mc, dec_mc, tc_refdet)
   
    for det in net:
        A22[det] = A22[det][idx_snrcut]
        h_dot_h[det] = h_dot_h[det][idx_snrcut]

    thr = max(phi_lkl_thr, lkl_thr)
    mag22_at_tc, phase22_at_tc = snr_at_tc(net, refdet, hp22_dot_d, series_times, tc_refdet_mc, dt_dets)
    h_dot_d = {}
    for det in net:
        h_dot_d[det] = A22[det] * mag22_at_tc[det] * np.exp(1j * phase22_at_tc[det]) 

    lkl = sky_likelihood(net, h_dot_h, h_dot_d, thr)
   
    return x[idx_snrcut], lkl

def get_likelihood_threshold(lkl, nsel, discard_prob):
    ''' Find the likelihood threshold that encolses a probability'''
    
    w = np.exp(lkl - np.max(lkl))
    npoints = len(w)
    sumw = np.sum(w)
    prob = w/sumw
    idx = np.argsort(prob)
    ecdf = np.cumsum(prob[idx])
    F = np.linspace(np.min(ecdf), 1., npoints)
    prob_stop_thr = lkl[idx][ecdf >= discard_prob][0]
    
    lkl_stop_thr = np.flip(np.sort(lkl))
    lkl_stop_thr = lkl_stop_thr[nsel]
    lkl_thr = min(lkl_stop_thr, prob_stop_thr)

    truncp = np.sum(w[lkl < lkl_thr]) / sumw
            
    return lkl_thr, truncp

def localize_in_volume(args, params, neffective, ncpu = 10):
    '''Localize the event in volume
       args: dictionary that contains information about the signal
       param_ranges: (dictionary) ranges of sky location where MC is focused
       start_lkl_thr: float starting threshold to select samples
       ncpu: number of cpu's to use in python multi-processing
    '''
    
    args = deepcopy(args)
    params = deepcopy(params)
    ncpu = min(ncpu, cpu_count() - 2)
    pool = Pool(processes = ncpu, maxtasksperchild = 100)

    nitr = 1000
    nsel = 8000
    ndim = 6
    V = 1
    at_final_threshold = False
    
    enc_prob = 0.999
    trunc_p = 1e-10
    
    nmc = 1000000 // ncpu + 1
    allx, lkl, phic = np.transpose([[]] * ndim), np.array([]), np.array([])
    params['ninbin'] = [nmc]
    params['binunique'] = np.array([ndim * [0]])
    xrange = params['xrange']
    params['dx'] = np.diff(xrange, axis = 1).flatten()
    fac = np.linspace(1, 2, ndim)
    fac *= (ndim/np.sum(fac))
    binscale = 0
   
    for itr in range(nitr):
   
        data = zip(ncpu * [args], ncpu * [params])
        results = pool.map(volume_localized, data)
   
        x_mc = np.vstack([r[0] for r in results])
        lkl_mc = np.hstack([r[1] for r in results])

        allx = np.append(allx, x_mc, axis = 0)
        lkl = np.append(lkl, lkl_mc)

        if itr > 0:
            accept = np.where(lkl > args['lkl_thr'])
            lkl = lkl[accept]
            allx = allx[accept]
        
        ninj = len(lkl)
        
        if not(at_final_threshold):
            args['lkl_thr'], truncp = get_likelihood_threshold(lkl, nsel, 1 - enc_prob - trunc_p)
            trunc_p += truncp
        at_final_threshold = np.round(enc_prob/trunc_p) - np.round(enc_prob/(1 - enc_prob)) == 0
        
        accept = np.where(lkl > args['lkl_thr'])
        allx = allx[accept]
        lkl = lkl[accept]
       
        maxl = np.max(lkl)
        w = np.exp(lkl - maxl)
        neff = np.sum(w) ** 2 / np.sum(w ** 2)
        if neff > neffective and len(lkl) > 200000:
            break
           
        nrec = len(lkl)
        V *= (nrec / ninj)
        delta_V = V / np.sqrt(nrec) 

        #nbins = int((1 / delta_V) ** (1 / ndim))
        nbins = get_bins(delta_V, fac, ndim)
        params['dx'] = dx = np.diff(xrange, axis = 1).flatten() / nbins
        binidx = ((allx - xrange.T[0]) / dx.T).astype(int)
        params['binunique'] = binunique = np.unique(binidx, axis = 0)
        params['ninbin'] = ((nmc // binunique.shape[0] + 1) * np.ones(binunique.shape[0])).astype(int)
        
        binscale += np.array([len(np.unique(bu)) for bu in binunique.T])/nbins
        fac = 1/np.sqrt(binscale/np.sum(binscale))
        fac *= (ndim/np.sum(fac))
       
        print (itr, nbins, allx.shape[0], np.round(neff, 1), np.round(maxl, 2), np.round(args['lkl_thr'], 2), np.round(trunc_p, 4))
   
    lumdcube, cosi, ra, sindec, tc_refdet, psi = allx.T
    dec = np.arcsin(sindec)
    extrinsic_mc = {}
    extrinsic_mc['log_likelihood'] = lkl
    extrinsic_mc['psi'] = psi
    extrinsic_mc['ra'], extrinsic_mc['dec'] = ra, dec
    extrinsic_mc['lumd'], extrinsic_mc['cosi'] = lumdcube ** (1./3), cosi
    extrinsic_mc['tc_refdet'] = tc_refdet
   
    pool.close()
    pool.join()
   
    return extrinsic_mc
       
def intrinsic_sampled(params_args_intrinsic):
   
    np.random.seed()
    args = deepcopy(params_args_intrinsic)
    args_intrin, params = args
   
    net = args_intrin['net']
    netsnr = args_intrin['netsnr']
    refdet = args_intrin['refdet']
    freqs = args_intrin['freqs']

    delta_tc = args_intrin['delta_tc']
    lkl_thr = args_intrin['lkl_thr']
    max_lkl_full = args_intrin['max_lkl_full']
    signal_tilde, psd = args_intrin['signal_tilde'], args_intrin['psd']
    flen = len(psd[net[0]])
    df = args_intrin['delta_f']
    phi_sky_idx = int(np.mean(freqs)/df)
   
    A22 = args_intrin['A22']
    A33 = args_intrin['A33']
    dt_dets_mc = args_intrin['dt_dets_mc']
    series_times = args_intrin['series_times']
    istart = args_intrin['istart']
    istop = args_intrin['istop']
    
    flow, fhigh = params['Freq']
    Psi0 = params['Psi0']
    
    max_dPsi1D = params['max_dPsi1D']
    dx = params['dx']
    xrange = params['xrange']
    bu = params['binunique']
    npercpu = params['npercpu']
   
    ndim = 4
    nseg = len(freqs) - 1
    allx = np.transpose([[]] * ndim)
    dPsi_mc, log_sumprob_mc, skyidx_mc = [], [], []
    tcs_refdet = []
    s22_max_det, s33_max_det = {}, {}

    nsky = len(dt_dets_mc[0][refdet])
    log_nsky = np.log(nsky)
    log_rndn = np.log(np.random.random(nsky))
   
    maxl_cycle = 0
    t0 = tnow = time.time()
    nbins = bu.shape[0]
    binidx = np.random.choice(range(nbins))
    ncoll, niter, skyiter = 0, 0, 0
    nskyiter = len(dt_dets_mc.keys())
    xlo, xhi = xrange.T[0] + dx * bu, xrange.T[0] + dx * (bu+1)
    
    while np.sign(tnow - t0 - 120.) + np.sign(ncoll - npercpu) < 2:
       
        niter += 1
    
        skyiter += 1
        skyiter = skyiter % nskyiter
       
        binidx += 1
        binidx = binidx % nbins
        
        x = np.random.uniform(xlo[binidx], xhi[binidx])
        mch, q, s1z, s2z = x
        
        m1, m2 = qmch_to_m1m2(mch, q)
        Psiatl = get_pn_phase(m1, m2, s1z, s2z, flow)
        PsiatFreq0 = Psiatl - get_pn_phase(m1, m2, s1z, s2z, fhigh)
        dPsi1D = np.abs(PsiatFreq0 - Psi0)
        if dPsi1D > max_dPsi1D:
            continue
           
        hp22_dot_hp22, hp33_dot_hp33 = {}, {}
        hp22_dot_d, hp33_dot_d = {}, {}
        
        hp22, _ = get_fd_waveform(approximant=apx, 
                mass1=m1, mass2=m2, spin1z=s1z, spin2z=s2z, 
                delta_f=df, f_lower=freqs[0], f_final = freqs[-1], mode_array=(2, 2))
        hp22.resize(flen)
        hp22 = hp22.data
        hp33, _ = get_fd_waveform(approximant=apx, 
                mass1=m1, mass2=m2, spin1z=s1z, spin2z=s2z, 
                delta_f=df, f_lower=freqs[0], f_final = freqs[-1], mode_array=(3, 3))
        hp33.resize(flen)
        hp33 = hp33.data

        raw_snr_lkl = 0
        for det in net:
            hp22_dot_hp22[det] = sigproc.sigmasq(hp22, psd[det], df)
            hp22_dot_d[det] = sigproc.matched_filter(hp22, signal_tilde[det], df)
            hp22_dot_d[det] = hp22_dot_d[det][istart:istop]#dont need the full series
            raw_snr_lkl += np.max(np.abs(hp22_dot_d[det])) ** 2 / hp22_dot_hp22[det]
            
            hp33_dot_hp33[det] = sigproc.sigmasq(hp33, psd[det], df)
            hp33_dot_d[det] = sigproc.matched_filter(hp33, signal_tilde[det], df)
            hp33_dot_d[det] = hp33_dot_d[det][istart:istop]
            if hp33_dot_hp33[det] > 0:
                raw_snr_lkl += np.max(np.abs(hp33_dot_d[det])) ** 2 / hp33_dot_hp33[det]
            
        raw_snr_lkl *= 0.5
        ss = np.abs(hp22_dot_d[refdet] / np.sqrt(hp22_dot_hp22[refdet]))
        if raw_snr_lkl < lkl_thr or np.max(ss) < 4:
            continue
        
        #get range for ccoalesence time
        argmax = np.argmax(ss)
        sssq = ss ** 2 - ss[argmax] ** 2
        idxsel = np.where(sssq > -16)
        idxl = max(0, min(argmax - 2, idxsel[0][0] - 1))
        idxr = min(len(sssq) - 1, max(argmax + 2, idxsel[0][-1] + 1))
    
        ssinterp = interp1d(series_times[idxl:idxr + 1], sssq[idxl:idxr + 1], kind='quadratic', bounds_error = False, fill_value = -25)
        ax = np.linspace(series_times[idxl], series_times[idxr], (idxr - idxl) * 10)
        ssin = ssinterp(ax)
        idxsel = np.where(ssin > -16)
        l, r = ax[max(0, idxsel[0][0] - 1)], ax[min(len(ax) - 1, idxsel[0][-1] + 1)]
        tc_refdet = np.random.uniform(l, r, nsky)

        #template_dot_d at different coalesence time
        mag22_at_tc, phase22_at_tc = snr_at_tc(net, refdet, hp22_dot_d, series_times, tc_refdet, dt_dets_mc[skyiter])
        mag33_at_tc, phase33_at_tc = snr_at_tc(net, refdet, hp33_dot_d, series_times, tc_refdet, dt_dets_mc[skyiter])
        h_dot_d, h_dot_h = {}, {}
        for det in net:
            h_dot_d[det] = A22[skyiter][det] * mag22_at_tc[det] * np.exp(1j * phase22_at_tc[det]) 
            h_dot_d[det] += A33[skyiter][det] * mag33_at_tc[det] * np.exp(1j * phase33_at_tc[det])
            
            h = A22[skyiter][det] * np.sqrt(hp22_dot_hp22[det]) + A33[skyiter][det] * np.sqrt(hp33_dot_hp33[det])
            h_dot_h[det] = sigproc.get_product(h, h)#alread divided by psd in hp22_dot_hp22
        lkl_sky = sky_likelihood(net, h_dot_h, h_dot_d, lkl_thr + log_nsky)
        lkl_sky += np.log((r - l)/delta_tc)
        lkl = logsumexp(lkl_sky) - log_nsky
            
        if lkl < lkl_thr:
                continue
            
        normed_lkl = lkl_sky - max_lkl_full
        skyidx = np.where(normed_lkl > log_rndn)[0]
        if len(skyidx) > 0:
            skyidx = np.random.choice(skyidx) 
            skyidx_mc = np.append(skyidx_mc, skyidx + skyiter*nsky)
            tcs_refdet = np.append(tcs_refdet, tc_refdet[skyidx])
        else:
            skyidx_mc = np.append(skyidx_mc, -1)
            tcs_refdet = np.append(tcs_refdet, 0)
        np.random.shuffle(log_rndn)
        
        allx = np.vstack([allx, x])
        dPsi_mc = np.append(dPsi_mc, dPsi1D)
        log_sumprob_mc = np.append(log_sumprob_mc, lkl)
        #maxl_cycle = max(maxl_cycle, np.max(lkl_sky))
        max_lkl_full = max(max_lkl_full, np.max(lkl_sky))
        
        ncoll += 1
        #if ncoll % 20 == 0:
        #    print (niter, ncoll, lkl, skyidx, sssq[idxl], sssq[idxr], max_lkl_full)
        
        tnow = time.time()

    return allx, dPsi_mc, log_sumprob_mc, skyidx_mc, tcs_refdet, niter, max_lkl_full#maxl_cycle

def sample_intrinsic(args, params, ncpu):

    data = zip(ncpu * [args], ncpu * [params])
    pool = Pool(processes = ncpu, maxtasksperchild = 100)
    results = pool.map(intrinsic_sampled, data)

    x_mc = np.vstack([r[0] for r in results])
    dPsi = np.concatenate([r[1] for r in results])
    lkl_mc = np.concatenate([r[2] for r in results])
    skyidx_mc = np.concatenate([r[3] for r in results])
    tc_refdet = np.concatenate([r[4] for r in results])
    niter = np.sum([r[5] for r in results])
    maxl_cycle = np.max([r[6] for r in results])
   
    intrinsic_mc = {}
    intrinsic_mc['x'] = x_mc
    intrinsic_mc['tc_refdet'] = tc_refdet
    intrinsic_mc['dPsi'] = dPsi
    intrinsic_mc['skyidx'] = skyidx_mc
    intrinsic_mc['log_likelihood'] = lkl_mc
    intrinsic_mc['niter'] = niter
    intrinsic_mc['maxl_cycle'] = maxl_cycle
   
    pool.close()
    pool.join()

    return intrinsic_mc

def get_pn_phase(m1, m2, s1z, s2z, f_pn_ref):
   
    unit_norm = 4.927e-6
    mchirp, eta = m1m2_to_mcheta(m1, m2)
    M, chis, chia, delta = m1 + m2, 0.5 * (s1z + s2z), 0.5 * (s1z - s2z), (m1 - m2)/(m1 + m2)
    nu, chi_pn = (np.pi * M * f_pn_ref * unit_norm) ** (1./3), chis + delta * chia - (76 * eta/113.) * chis
   
    psi1 = nu ** 2 * (3715./756 + 55.*eta/9) + nu ** 3 * (113.*chi_pn/3 - 16*np.pi)

    prefac = (3./128/eta/nu ** 5)
   
    return prefac * (1 + psi1)

def sample_full_pe(ts_ft, psd, gps, template_param, freqs,  ncpu):
   
    # Get template waveform
    Dt = 0.1
    neff, neffective = 0, 10000

    V = 1
    itr = 0
    nsel = 1000
    start_time = time.time()
    intrinsic_mc = {}
    trunc_p = 1e-10
    at_final_threshold = False
    
    args_extrin = assemble_args_extrinsic(ts_ft, template_param, psd, freqs, gps, Dt)
    params_extrin = assemble_param_ranges_extrinsic(args_extrin)

    extrinsic_mc = localize_in_volume(args_extrin, params_extrin, 1000, ncpu = ncpu)
    gps = extrinsic_mc['tc_refdet'][np.argmax(extrinsic_mc['log_likelihood'])]
    args_intrin = assemble_args_intrinsic(args_extrin, extrinsic_mc, template_param)
    #return params_extrin
    
    ndim = 4
    params_intrin = assemble_param_ranges_intrinsic(args_extrin, template_param)
    params_intrin['ninbin'] = [1]
    params_intrin['npercpu'] = nsel//ncpu + 1
    params_intrin['binunique'] = np.array([ndim * [0]])
    xrange = params_intrin['xrange']
    params_intrin['dx'] = np.diff(xrange, axis = 1).flatten()
    params_intrin['binscale'] = 0
    
    #return extrinsic_mc, params_extrin, args_extrin
    #return args_intrin, params_intrin

    while neff < neffective:
        key = itr
       
        intrinsic_mc[key] = sample_intrinsic(args_intrin, params_intrin, ncpu = ncpu)
        args_intrin, params_intrin, all_lkl, neff, V, nbins, trunc_p, at_final_threshold = \
                        override_intrinsic_ranges(args_intrin, params_intrin, intrinsic_mc, nsel, V, trunc_p, at_final_threshold)
       
        print (itr, ')', np.round(template_param, 4))
        print ('Nbins:', nbins)
        print ('max dpsi', params_intrin['max_dPsi1D'])
        print (np.round(neff, 2), np.round(args_intrin['max_lkl_full'], 2), np.round(np.max(all_lkl), 2), np.round(args_intrin['lkl_thr'], 2), len(all_lkl), np.round(trunc_p, 6))
        print ()
    
        itr += 1
       
    print("---- %s seconds ----" % (time.time() - start_time))
   
    return extrinsic_mc, args_extrin, params_extrin, intrinsic_mc, args_intrin, params_intrin, time.time() - start_time

def collect_posterior(extrinsic_mc, intrinsic_mc, lkl_thr):
   
    mchirp, q, spin1z, spin2z = [], [], [], []
    ra, dec, lumd, cosi = [], [], [], []
    lkl, dPsi, tc_refdet = [], [], []
    phic, psi = [], []
    lkl_extr = []
    keys = list(intrinsic_mc.keys())
    all_maxl = np.array([intrinsic_mc[key]['maxl_cycle'] for key in keys])
    key_first = np.where(all_maxl + 0.01 > np.max(all_maxl))[0][0]
    for key in keys:
        
        if key < key_first:
            continue
            
        idxsel = np.where((intrinsic_mc[key]['log_likelihood'] > lkl_thr) & (intrinsic_mc[key]['skyidx'] > -1))
        mch, qq, s1z, s2z = intrinsic_mc[key]['x'].T
        mchirp = np.append(mchirp, mch[idxsel])
        q = np.append(q, qq[idxsel])
        spin1z = np.append(spin1z, s1z[idxsel])
        spin2z = np.append(spin2z, s2z[idxsel])
       
        skyidx = intrinsic_mc[key]['skyidx'][idxsel].astype(int)
        ra = np.append(ra, extrinsic_mc['ra'][skyidx])
        dec = np.append(dec, extrinsic_mc['dec'][skyidx])
        lumd = np.append(lumd, extrinsic_mc['lumd'][skyidx])
        cosi = np.append(cosi, extrinsic_mc['cosi'][skyidx])
        psi = np.append(psi, extrinsic_mc['psi'][skyidx])
       
        lkl = np.append(lkl, intrinsic_mc[key]['log_likelihood'][idxsel])
        dPsi = np.append(dPsi, intrinsic_mc[key]['dPsi'][idxsel])
        tc_refdet = np.append(tc_refdet, intrinsic_mc[key]['tc_refdet'][idxsel])
   
    full_pe = {}
    full_pe['mchirp'] = mchirp
    full_pe['q'] = q
    full_pe['spin1z'] = spin1z
    full_pe['spin2z'] = spin2z
    full_pe['ra'] = ra
    full_pe['dec'] = dec
    full_pe['lumd'] = lumd
    full_pe['cosi'] = cosi
    full_pe['psi'] = psi
   
    full_pe['log_likelihood'] = lkl
    full_pe['dPsi'] = dPsi
    full_pe['tc_refdet'] = tc_refdet
   
    return full_pe

def applied_weights_to_loglkl(loglkl, nsel, nmc):
    
    lw = loglkl - np.max(loglkl)
    iexps = 1/np.exp(np.linspace(0, np.log(nmc), 10))
    for ii in range(4):
        a, b = [], []
        for kk, iexp in enumerate(iexps):
            rw = np.exp(iexp * lw)
            redneff = np.sum(rw)
            if redneff > nsel:
                break
        l = -np.log(iexps[max(0, kk-1)])
        r = -np.log(iexps[kk])
        iexps = 1/ np.exp(np.linspace(l, r, 10))
    
    return rw, iexp

def get_bins(delta_V, fac, ndim):
    maxbin = fac * (1/delta_V) ** (1/ndim)
    nbinmin = np.maximum((maxbin - maxbin**0.25).astype(int), 1)
    nbinmax = np.maximum((maxbin + maxbin**0.25).astype(int) + 1, 4)
    itrbins = np.random.randint(nbinmin, nbinmax, size= (100000, ndim))
    pbins = np.exp(np.sum(np.log(itrbins), axis=1))
    nbins = itrbins[np.argmin(np.abs(1/delta_V - pbins))]
        
    return nbins

