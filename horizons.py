from multiprocessing import Pool, cpu_count
from pycbc.waveform import get_fd_waveform
from pycbc.filter import sigmasq
from pycbc import waveform
import numpy as np
import h5py

from conversions import *

ncpu = 85 #cpu_count()
mchmin, mpow, nmch = 5.0, 1.02, 170 # Using geometric progression now
qmin, qmax, dq = 0.1, 1.0, .02
szmin, szmax, dsz = -.9, 0.9, .02

def save_horizons(psd, det):
    '''
    Save the horizon distance for aligo type detector. Covered by chirp mass, mass ratio and effective spin of the binary
    '''  

    mchirps = mchmin *  mpow ** np.arange(nmch) # Using a geometric series
    pool = Pool(processes = ncpu)
    results = pool.map(horizon_for_a_mchirp, zip(mchirps, len(mchirps) * [psd]))
    
    fname = "Data/advanced_detectors_horizons_" + det + ".h5py"
    print ("Saving to file: " + fname)
    nq, nsz = int((qmax - qmin)/dq)+1, int((szmax - szmin)/dsz)+1
    ndata = nmch * nq * nsz * nsz
 
    infile = h5py.File(fname, "w")
    dset1 = infile.create_dataset("mchirp", (ndata,), dtype='f', maxshape=(ndata,))
    dset2 = infile.create_dataset("q", (ndata,), dtype='f', maxshape=(ndata,))
    dset3 = infile.create_dataset("spin1z", (ndata,), dtype='f', maxshape=(ndata,))  
    dset4 = infile.create_dataset("spin2z", (ndata,), dtype='f', maxshape=(ndata,))
    dset5 = infile.create_dataset("d_horizon", (ndata,), dtype='f', maxshape=(ndata,))
    dset6 = infile.create_dataset("dphi100hz", (ndata,), dtype='f', maxshape=(ndata,))

    dset1.attrs['mchmin'] = mchmin
    dset1.attrs['mpow'] = mpow
    dset1.attrs['nmch'] = nmch
    dset2.attrs['qmin'] = qmin
    dset2.attrs['qmax'] = qmax
    dset2.attrs['dq'] = dq
    dset3.attrs['spinzmin'] = szmin
    dset3.attrs['spinzmax'] = szmax
    dset3.attrs['dsz'] = dsz
        
    dset1[:] = np.concatenate([yy[0] for yy in results])
    dset2[:] = np.concatenate([yy[1] for yy in results])
    dset3[:] = np.concatenate([yy[2] for yy in results])
    dset4[:] = np.concatenate([yy[3] for yy in results])
    dset5[:] = np.concatenate([yy[4] for yy in results])
    dset6[:] = np.concatenate([yy[5] for yy in results])

    infile.close()
    
def horizon_for_a_mchirp(args):
    
    mchirp, psd = args
    df, fl, fh = psd.delta_f, np.where(psd.data != 0)[0][0] * psd.delta_f, psd.delta_f * len(psd) - psd.delta_f
    f, fstart, fstop = np.array(psd.sample_frequencies), int(fl/df), int(fh/df)
    i50, i100 = int(50/psd.delta_f), int(100/psd.delta_f)
    
    hp_zs, hc_zs = get_waveform(mchirp * 2 ** 0.2, mchirp ** 2 ** 0.2, 0., 0., df, fl, fh)
    phi_zs = waveform.utils.phase_from_frequencyseries(hp_zs)
    #zs: zero spin, 2 ** 0.2 takes chirp mass to equal mass binary
    
    print (np.round(mchirp, 2))
    
    all_mch, all_q, all_s1z, all_s2z, all_dhr, all_dphi = [], [], [], [], [], []
    for q in np.arange(qmin, qmax + dq / 2., dq):

        m1, m2 = qmch_to_m1m2(mchirp, q)
        
        for s1z in np.arange(szmin, szmax + dsz / 2., dsz):
            for s2z in np.arange(szmin, szmax + dsz / 2., dsz):

                hp, hc = get_waveform(m1, m2, s1z, s2z, df, fl, fh)
                dhr = d_horizon(hp, psd, fl)      
            
                phi = waveform.utils.phase_from_frequencyseries(hp)
                dphi = (phi[i100] - phi[i50]) - (phi_zs[i100] - phi_zs[i50])

                all_mch.append(mchirp)
                all_q.append(q)
                all_s1z.append(s1z)
                all_s2z.append(s2z)
                all_dhr.append(dhr)
                all_dphi.append(dphi)
    
    return [all_mch, all_q, all_s1z, all_s2z, all_dhr, all_dphi]
    
def get_waveform(m1, m2, s1, s2, df, f_low, f_high, d = 1):
    sptilde, sctilde = get_fd_waveform(approximant="IMRPhenomD",
                    mass1=m1, mass2=m2, spin1z = s1, spin2z = s2, delta_f = df, 
                                       f_lower=f_low, f_final = f_high, distance = d)
    return sptilde, sctilde

def d_horizon(hp, psd, f_low, f_high):
    '''
    Get horizon distance for a input waveform and noise
    '''
    sq = np.sqrt(sigmasq(hp, psd, low_frequency_cutoff = f_low, high_frequency_cutoff = f_high))
    return sq/8. # at SNR = 8
