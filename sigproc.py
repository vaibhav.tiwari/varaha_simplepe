import numpy as np

def sigmasq(hptilde, psd, df):
    '''PSD weighted inner product of template'''
    
    return 4 * df * np.real(np.sum(hptilde * np.conjugate(hptilde) / psd))

def get_product(a, b):
    '''PSD weighted inner product of template'''
    
    return np.real(a * np.conjugate(b))


def matched_filter(hptilde, sptilde, df):
    ''' Matched filter of data with a template'''
    
    mf = np.conjugate(hptilde) * sptilde
    mf.resize(2 * len(mf) - 2)
    norm = (4.0 * df)
    
    return norm * len(mf) * np.fft.ifft(mf)
    
    