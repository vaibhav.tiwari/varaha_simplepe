import numpy as np
from numpy import pi
from scipy.stats import chi2
from astropy import units as unt
from astropy.cosmology import Planck15 as cosmo

from pycbc import waveform
from pycbc.detector import Detector

from collections import OrderedDict
from multiprocessing import Pool, cpu_count

from conversions import *
from models import *
import detectors, horizons

min_found_netsnr = 6.
f_low, f_high = 20., 1024.

class observations(object):
    """
    class to hold sampled observations from a model. An event if considered found 
    if it has a network SNR of at least 11 and per detector SNR of at least 5.
    """

    def __init__(self, network, mass_model, starformation_model, **kwargs):
        """
        :mass_model: Model as stored in the models.py file e.g. models.imf(1)
        """
    
        self.mass_model = mass_model
        self.nsamples = kwargs.get('nsamples', "")
        
        m1, m2, s1z, s2z, z, ra, dec, psi, phi, cosi, snr = generate_obs(
                                network, mass_model, starformation_model, **kwargs)
        
        self.z = z
        self.m1 = m1
        self.m2 = m2
        self.s1z = s1z
        self.s2z = s2z
        self.ra = ra
        self.dec = dec
        self.psi = psi
        self.phi = phi
        self.cosi = cosi
        self.snr = snr
        
def get_significant(snr_dict, min_net_snr, min_det_snr, nmin_det_snr):
    ''' Get significant event from the tupule of events
        min_net_snr: Minimum network SNR
        min_det_snr: Minimum per detector SNR
        nmin_det_snr: Number of detectors to have min_det_snr
    '''
    
    assert min_net_snr >= min_found_netsnr, 'Minimum network SNR should be more than ' + str(min_found_netsnr)

    net = list(snr_dict.keys())
    snr_sq, nmin_snr = 0, np.zeros_like(snr_dict[net[0]])
    for det in net:
        snr_sq += snr_dict[det] ** 2
        nmin_snr[snr_dict[det] > min_det_snr] += 1
    netsnr = np.sqrt(snr_sq)
    
    idx = np.where((netsnr >= min_net_snr) & (nmin_snr >= nmin_det_snr))
    
    return idx

def generate_obs(net, mass_model, starformation_model, **kwargs):
    """
    Generate observations based on provided model.
    """
    nsamples = kwargs.get('nsamples')
    z_max = kwargs.get('z_max')
    psd = kwargs.get('psd')
    m1, m2 = mass_model(**kwargs)
    s1z, s2z = draw_astro_spinz(**kwargs)

    zs = astro_redshifts(starformation_model, **kwargs)
    
    ra, dec = generate_extrinsic_sky(nsamples)
    psi, phi = generate_extrinsic_phasing(nsamples)
    _, cosi = generate_extrinsic_loc(nsamples) # lumd is not needed
    lumd = z_to_dlum(zs)
    norm_det = get_norm_det(net, ra, dec, psi, phi, cosi, t_gps = 0)
    
    for det in net:
        norm_det[det] = np.abs(norm_det[det])

    m1_det, m2_det = m1 * (1 + zs), m2 * (1 + zs)
    idx_hopeful = get_hopeful(net, m1_det, m2_det, s1z, s2z, lumd, norm_det, psd)
    
    norm_det_sel = {}
    for det in net:
        norm_det_sel[det] = norm_det[det][idx_hopeful]
    snr = {}
    snr_hopeful = get_snr(net, m1_det[idx_hopeful], m2_det[idx_hopeful], 
                  s1z[idx_hopeful], s2z[idx_hopeful], lumd[idx_hopeful], norm_det_sel, psd)
            
    for det in net:
        snr[det] = np.zeros_like(m1)
        snr[det][idx_hopeful] = snr_hopeful[det]
    
    return m1, m2, s1z, s2z, zs, ra, dec, psi, phi, cosi, snr

def get_sfr_pdf(sfr_model, zs, **kwargs):
    ''' Get the probability density for the rate of events at a redshift 
        assuming standard cosmology and star formation model
        sfr_model: defined in models.py
        zs: redshift at which p(zs) is needed
        z_max: maximum redshift for the analysis
    '''
    
    z_max = kwargs.get('z_max')
    normalise = kwargs.get('normalise', True)
    dNdz = z_to_dcovdz(zs) * sfr_model(zs, **kwargs) / (1 + zs)
    if normalise:
        norm_sfr = get_dNdz_norm(sfr_model, **kwargs)
        return dNdz / norm_sfr
    
    return dNdz

def get_dNdz_norm(sfr_model, **kwargs):
    
    ''' Get the number of mergers for a star formation model 
        sfr_model: defined in models.py
    '''
    z_max = kwargs.get('z_max')
    zax = np.expm1(np.linspace(np.log(1.), np.log(1. + z_max), 3000))
    zax[-1] = z_max #above expression does not always end z_max due to precession
    updf_sfr = sfr_model(zax, **kwargs)
    dNdz = updf_sfr * z_to_dcovdz(zax) / (1 + zax)
    norm_sfr = np.trapz(dNdz, zax)
    
    return norm_sfr

def astro_redshifts(sfr_model, **kwargs):
    ''' Sample the redshifts for sources, with a starformation rate, using given cosmology'''
    
    z_max = kwargs.get('z_max')
    nsamples = kwargs.get('nsamples')
    
    
    zax = np.expm1(np.linspace(np.log(1.), np.log(1. + z_max), 3000))
    zax[-1] = z_max
    czax = 0.5 * (zax[1:] + zax[:-1])
    pdfz = get_sfr_pdf(sfr_model, czax,  **kwargs)
    cdfz = np.cumsum(pdfz * np.diff(zax))

    #Inverse sampling returns some spurious values
    rndp = np.random.uniform(0., 1., int(nsamples * 1.1)) 
    zastro = np.interp(rndp, cdfz, czax)
    zastro = zastro[(zastro > 0) & (zastro < z_max)]
    
    return np.resize(zastro, nsamples)

def get_snr(net, mass1, mass2, spin1z, spin2z, lumd, norm_det, psd):
    ''' Estimate the SNR in the detectors of the network
        All inputs are in the detector frame
    ''' 
    
    sigma, snr = {}, {}
    nsamples = len(mass1)
    for det in net:
        snr[det] = np.zeros(nsamples)
        n = range(nsamples)
        for i, m1, m2, s1z, s2z, d in zip(n, mass1, mass2, spin1z, spin2z, lumd):
            snr_faceon = detectors.snr_faceon_1mpc(m1, m2, s1z, s2z, psd[det], f_low, f_high)
            snr[det][i] = snr_faceon * norm_det[det][i] / d
    
    return snr

def get_hopeful(net, m1, m2, s1z, s2z, d, norm_det, psd):
    ''' Find hopeless injections using crude range estimation
    ''' 
    
    netsnrsq = 0
    mch, _ = m1m2_to_mcheta(m1, m2)
    mch_bns, _ = m1m2_to_mcheta(1.4, 1.4)
    fac_mass = (mch/mch_bns) ** (5./6)
    for det in net:
        snr = detectors.snr_faceon_1mpc(1.4, 1.4, 0, 0, psd[det], f_low, f_high) * norm_det[det] / d
        netsnrsq += (snr ** 2)
    
    netsnr = np.sqrt(netsnrsq) * fac_mass
    
    return np.where(netsnr > min_found_netsnr)
# Spins can increase the SNR.

def get_norm_det(net, ra, dec, psi, phi, cosi, t_gps):
    ''' Return the normalization to scale SNR from face-on to random sky-location for the network'''
    f_plus, f_cross = detectors.get_fp_fc(net, ra, dec, psi, t_gps)
    norm_det = detectors.norm_optsnr(net, phi, cosi, f_plus, f_cross)
    
    return norm_det

def generate_extrinsic_sky(n = 1, ra_min = 0, ra_max = 2*pi, sindec_min = -1, sindec_max = 1):
    """
    Generate extrinsic parameters of an event
    """
    ra = np.random.uniform(ra_min, ra_max, size = n)
    dec = np.arcsin(np.random.uniform(sindec_min, sindec_max, size = n))
    
    return ra, dec

def generate_extrinsic_phasing(n = 1, psi_min = 0, psi_max = 2*pi, phi_min = 0, phi_max = 2*pi):
    """
    Generate extrinsic parameters of an event
    """
    psi = np.random.uniform(psi_min, psi_max, size = n)
    phi = np.random.uniform(phi_min, phi_max, size = n)
    
    return psi, phi

def generate_extrinsic_loc(n = 1, lumd_min = 0, lumd_max = 10000, cosi_min = -1, cosi_max = 1):
    """
    Generate extrinsic parameters of an event
    """
    lumd = (np.random.uniform(lumd_min ** 3, lumd_max ** 3, size = n)) ** (1/3.)
    cosi = np.random.uniform(cosi_min, cosi_max, size = n)
    
    return lumd, cosi

def get_netsnr(snr):
    ''' Calculate network snr from detector snr's '''
    
    netsnr = 0
    for key in snr.keys():
        netsnr += snr[key] ** 2
        
    return np.sqrt(netsnr)

def map_sfr_in_lumd(sfr_model, z, **kwargs):
    ''' Map star formation p(z) to p(d_lum)'''
    
    pz = sfr_model(z, **kwargs) / get_dLdz(z)
    
    return pz

def map_sfrz_to_lumd(sfr_model, z, **kwargs):
    ''' Map p(z) to p(d_lum)'''
    
    if 'dd_dz' in kwargs:        
        dd_dz = kwargs.get('dd_dz')
    else:
        dd_dz = get_dLdz(z)
    
    pz = get_sfr_pdf(sfr_model, z, **kwargs) / dd_dz
    
    return pz

def get_detector_frame_quantities(obs, significant_idx, event_id):
    ''' Generate Signals expected at the detectors in a network
        obs: class object that holds all the observations produced from a population
        significant_idx (array): significant events in the observed events
        event_id (int): Event id within significant events.
    '''
    idx, ii = significant_idx, event_id
    z = obs.z[idx][ii]
    m1_det, m2_det = obs.m1[idx][ii] * (1 + z), obs.m2[idx][ii] * (1 + z)
    s1z, s2z = obs.s1z[idx][ii], obs.s2z[idx][ii]
    lumd = z_to_dlum(z)
    
    return m1_det, m2_det, s1z, s2z, lumd

def get_injection_extrinsic_params(obs, significant_idx, event_id):
    ''' Extract the extrinsic parameters from the observation class for an event/injection'''
    
    idx, ii = significant_idx, event_id
    ra, dec = obs.ra[idx][ii], obs.dec[idx][ii]
    psi, phi, inc = obs.psi[idx][ii], obs.phi[idx][ii], np.arccos(obs.cosi[idx][ii])
    
    return ra, dec, psi, phi, inc

def generate_signals(obs, significant_idx, event_id, net, f_low = 20., sampling_rate = 2048., seglen = 32, apx = 'IMRPhenomD'):
    ''' Generate Signals expected at the detectors in a network
        obs: class object that holds all the observations produced from a population
        significant_idx (array): significant events in the observed events
        event_id (int): Event id within significant events.
    '''
    
    dt = 1. / sampling_rate
    npoints = int(sampling_rate * seglen)
    m1_det, m2_det, s1z, s2z, lumd = get_detector_frame_quantities(obs, significant_idx, event_id)
    ra, dec, psi, phi, inc = get_injection_extrinsic_params(obs, significant_idx, event_id)

    sp, sc = waveform.get_td_waveform(approximant=apx, mass1=m1_det, mass2=m2_det, 
                                                           spin1z=s1z, spin2z=s2z, 
                                       inclination=inc, coa_phase=phi, delta_t=dt, 
                                                                    f_lower=f_low, 
                                                                  distance = lumd)
    #print sp.duration
    assert npoints >= len(sc), "Segment length too short for the signal!"
    right_ascension = ra
    declination = dec
    polarization = psi

    signal = {}
    for det in net:
        det_det = Detector(det)

        signal[det] = det_det.project_wave(sp, sc,  ra, dec, polarization)
        signal[det].resize(int(len(signal[det]) + npoints / 4.))
        signal[det].prepend_zeros(npoints - len(signal[det]))
        
    return signal

def generate_obs_for_volume(args):
    """
    Generate observations for the estimation of sensitive volume.
    """
    
    np.random.seed()
    
    net = args['net']
    psd = args['psd']
    mchpl_range = args['mchpl_range']
    alpha_mch = args['alpha_mch']
    min_q = args['min_q']
    alpha_q = args['alpha_q']
    max_spin  = args['max_spin']
    z_max = args['z_max']
    nsamples = args['nsamples']
    
    sfr_model = args['sfr_model']
    del args['sfr_model']

    mchirp_src = sample_broken_powerlaw(mchpl_range, alpha_mch, nsamples)
    mchirp_src[mchirp_src  < mchpl_range[0]] = mchpl_range[0]
    q = powerlaw_samples(min_q, 1., alpha_q, nsamples)
    min_sz, max_sz = -max_spin, max_spin
    s1z = np.random.uniform(min_sz, max_sz, nsamples)
    s2z = np.random.uniform(min_sz, max_sz, nsamples)
    zs = astro_redshifts(sfr_model, **args)
    lumd = z_to_dlum(zs)
    
    m1_src, m2_src = qmch_to_m1m2(mchirp_src, q)
    m1, m2 = (1 + zs) * m1_src, m2_src * (1 + zs)

    ra, dec = generate_extrinsic_sky(nsamples)
    psi, phi = generate_extrinsic_phasing(nsamples)
    _, cosi = generate_extrinsic_loc(nsamples)

    norm_det = get_norm_det(net, ra, dec, psi, phi, cosi, t_gps = 0)
    for det in net:
        norm_det[det] = np.abs(norm_det[det])    
    idx_hopeful = get_hopeful(net, m1, m2, s1z, s2z, lumd, norm_det, psd)
    #print len(idx_hopeful[0])
    
    norm_det_sel = OrderedDict()
    for det in net:
        norm_det_sel[det] = norm_det[det][idx_hopeful]
    snr = OrderedDict()
    snr_hopeful = get_snr(net, m1[idx_hopeful], m2[idx_hopeful], 
                  s1z[idx_hopeful], s2z[idx_hopeful], lumd[idx_hopeful], norm_det_sel, psd)
            
    for det in net:
        snr[det] = np.zeros_like(m1)
        snr[det][idx_hopeful] = snr_hopeful[det]

    injections_pdf = broken_powerlaw_pdf(mchirp_src, mchpl_range, alpha_mch)
    injections_pdf *= powerlaw_pdf(q, min_q, 1., alpha_q)
    injections_pdf /= (max_sz - min_sz) ** 2 
    injections_pdf *= get_sfr_pdf(sfr_model, zs, **args)
    
    return m1_src, m2_src, s1z, s2z, zs, ra, dec, psi, phi, cosi, [snr], injections_pdf

def obs_for_volume(args, ncpu = 18):
    
    ncpu = min(ncpu, cpu_count() - 2)
    pool = Pool(processes = ncpu, maxtasksperchild = 100)
    
    results = pool.map(generate_obs_for_volume, ncpu * [args])
    
    m1 = np.concatenate([yy[0] for yy in results])
    m2 = np.concatenate([yy[1] for yy in results])
    s1z = np.concatenate([yy[2] for yy in results])
    s2z = np.concatenate([yy[3] for yy in results])
    lumd = np.concatenate([yy[4] for yy in results])
    ra = np.concatenate([yy[5] for yy in results])
    dec = np.concatenate([yy[6] for yy in results])
    psi = np.concatenate([yy[7] for yy in results])
    phi = np.concatenate([yy[8] for yy in results])
    cosi = np.concatenate([yy[9] for yy in results])
    injections_pdf = np.concatenate([yy[11] for yy in results])
    
    snr = OrderedDict()
    net = args['net']
    for det in net:
        snr[det] = []
    for yy in results:
        for det in net:
            snr[det] = np.append(snr[det], yy[10][0][det])
    
    pool.close()
    pool.join()
    
    return m1, m2, s1z, s2z, lumd, ra, dec, psi, phi, cosi, snr, injections_pdf
